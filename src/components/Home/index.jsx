import { Link, useHistory } from "react-router-dom"
import "./style.css"

export default function Home({ members }) {
    const history = useHistory()
    return (
        <div>
            <nav>
                {members.map(item => {
                    if (item.type === "pj") {
                        return <Link key={item.id} to={`/company/${item.id}`}>{item.name}</Link>
                    } else {
                        return <Link key={item.id} to={`/customer/${item.id}`}>{item.name}</Link>
                    }
                })}
            </nav>
            <button onClick={() => history.push("./add_members")}>Add Members</button>
        </div>
    )
}