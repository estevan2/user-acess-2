import { useHistory } from "react-router-dom"
import { useState } from "react"
import "./style.css"

export default function AddMembers({ addMember, membersList }) {

    const [name, setName] = useState('')
    const [company, setCompany] = useState('')
    const [isAdded, setIsAdded] = useState(false)
    const [push, setPush] = useState(false)
    const [isNot, setIsNot] = useState(false)
    const msg = ['Membro Adicionado', "Nome já cadastrado ou Pessoa Juridíca/Física não selecionada!"]

    const history = useHistory()

    const handleHome = () => history.push('./')

    const verify = (person, type) => {
        if (membersList.find((item) => item.name === person)) {
            return false
        }
        if (type === "") {
            return false
        }

        return true
    }

    const captureMember = () => {
        setPush(false)
        if (verify(name, company)) {
            addMember([...membersList, {
                id: `${membersList.length + 1}`,
                name: name,
                type: company,
            }])
            setPush(true)
            setIsAdded(true)
            setIsNot(false)
        } else {
            setPush(true)
            setIsNot(true)
            setIsAdded(false)
        }
    }

    return (
        <div>
            <h1>Adicionar Membro</h1>
            <input value={name} type="text" placeholder="Nome" onChange={(event) => setName(event.target.value)} />
            <select onChange={(e) => setCompany(e.target.value)}>
                <option>Pessoa Física ou Jurídica</option>
                <option value="pf">Pessoa Física</option>
                <option value="pj">Pessoa Jurídica</option>
            </select>
            <div className="buttons">
                <button className="add" onClick={captureMember}>Adicionar Membro</button>
                <button className="back" onClick={handleHome}>Voltar</button>
            </div>
            <p>{push && isAdded && !isNot ? msg[0] : ''}</p>
            <p>{push && !isAdded && isNot ? msg[1] : ''}</p>
        </div>
    )
}