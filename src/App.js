import './App.css';
import Home from "./components/Home"
import { members } from "./components/Members"
import Custumer from './pages/Custumer';
import Company from './pages/Company';
import { Switch, Route } from 'react-router-dom';
import { useState } from 'react'
import AddMembers from './components/AddMembers';

function App() {

  const [membersList, setMembersList] = useState(members)
  console.log(membersList)

  return (
    <div className="App">
      <main className="App-header">
        <Switch>
          <Route exact path="/customer/:id">
            <Custumer members={membersList} />
          </Route>
          <Route exact path="/company/:id">
            <Company members={membersList} />
          </Route>
          <Route exact path="/">
            <Home members={membersList} />
          </Route>
          <Route exact path="/add_members">
            <AddMembers addMember={setMembersList} membersList={membersList} />
          </Route>
        </Switch>
      </main>
    </div>
  );
}

export default App;
